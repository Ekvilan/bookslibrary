-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.36 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;


-- Дамп структуры для таблица library.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `realname` varchar(100) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `books_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.books: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
REPLACE INTO `books` (`id`, `name`, `realname`, `author`, `category`, `extension`, `upload_date`, `user_id`, `description`) VALUES
	(10, 'гибель богов', '1- Гибель богов.txt', 'перумов', 'fantasy', 'txt', '2014-09-02', 1, NULL),
	(11, 'воин великой тьмы', '2 - Воин великой тьмы.txt', 'перумов', 'fantasy', 'txt', '2014-09-02', 1, NULL),
	(12, 'земля без радости', '3 - Земля без радости.txt', 'перумов', 'fantasy', 'txt', '2014-09-02', 1, NULL),
	(18, 'адамант хены', 'Адамант Хены.txt', 'перумов', 'fantasy', 'txt', '2014-09-03', 1, NULL);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;


-- Дамп структуры для таблица library.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `user_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.comment: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
REPLACE INTO `comment` (`id`, `content`, `user_id`, `book_id`, `date`) VALUES
	(2, 'fghfgh', 1, 11, NULL),
	(3, 'fghfgh', 1, 12, NULL),
	(4, 'this is the best book i\'ve ever read!!!', 1, 10, '2014-09-03'),
	(5, 'great book!!!', 2, 10, '2014-09-03'),
	(6, 'sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s   sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s   sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s   sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s  sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s sdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh ssdkjfhksjd sdkfh ksjdhf hkjs hdfkjshd hskdjfh kjsdhf kjhsdfjkh s ', 1, 10, '2014-09-03'),
	(7, 'bad, very bad.', 3, 10, '2014-09-03'),
	(8, '"to test" you\'re right', 4, 10, '2014-09-03');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;


-- Дамп структуры для таблица library.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.role: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
REPLACE INTO `role` (`id`, `role`, `user_id`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 2, 3),
	(4, 2, 4),
	(5, 2, 5),
	(6, 2, 6);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Дамп структуры для таблица library.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.users: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `registration_date`) VALUES
	(1, 'admin', 'admin', '2014-01-09'),
	(2, 'user', 'user', '2014-09-01'),
	(3, 'test', 'test', '2014-09-01'),
	(4, 'test2', 'test', '2014-09-01'),
	(5, 'test3', 'test', '2014-09-01'),
	(6, 'dima', 'dima', '2014-09-03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
