-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.33 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;


-- Дамп структуры для таблица library.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `realname` varchar(100) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` text,
  `image_path` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `books_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.books: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
REPLACE INTO `books` (`id`, `name`, `realname`, `author`, `category`, `extension`, `upload_date`, `user_id`, `description`, `image_path`) VALUES
	(9, 'рождение мага', '6 - Рождение мага.txt', 'перумов', 'arts & photography', 'txt', '2014-09-12', 32, NULL, NULL),
	(10, 'странствия мага', '7 - Странствия мага.txt', 'перумов', 'detective', 'txt', '2014-09-12', 32, NULL, NULL),
	(11, 'как перестать беспокоиться', 'Kak_perestatb_bespokoitbsja_i_nachatb_zhitb.txt', 'неизвестно', 'biographies & memoirs', 'txt', '2014-09-12', 33, NULL, 'd:\\ProjectRepository\\BooksLibrary\\images\\zI1As1.jpg');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;


-- Дамп структуры для таблица library.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `user_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.comment: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
REPLACE INTO `comment` (`id`, `content`, `user_id`, `book_id`, `Date`) VALUES
	(27, 'test', 32, 9, '2014-09-12'),
	(28, 'тест тест тест\r\n', 32, 9, '2014-09-12'),
	(29, 'просто тест', 33, 10, '2014-09-12');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;


-- Дамп структуры для таблица library.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `FK_svdv2jq4dkan56efhtqpl0wgw` (`user`),
  CONSTRAINT `FK_svdv2jq4dkan56efhtqpl0wgw` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.role: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
REPLACE INTO `role` (`id`, `role`, `user_id`, `user`) VALUES
	(27, 1, 30, NULL),
	(28, 1, 31, NULL),
	(29, 2, 32, NULL),
	(30, 2, 33, NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Дамп структуры для таблица library.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  `registration_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `registration_date`) VALUES
	(30, 'administrator', '767e3689036b2e6716d7321d63ea9a211464d8302f6ccde582d19825bfee98ed0a66d369993d45f79e7f180b03185553e936e5f35180052d166233f78cff3e7670d2653171ec0388', '2014-09-12'),
	(31, 'admin', 'a139a3c88618fcda6d147cc5f89e5c34e7598f78deb9ef8e358e109b6925194649a09a6e92ec7b9b977bca8dd8ad0a02f7b3082350741f512f568b934269a1d944f819e3d6d577d4', '2014-09-12'),
	(32, 'user', '0d784455e717f88f1a7dfade2ec4cf760d8c889e31bc5c2b2833c603219117c4dd5ffd1635834980c7eb25055115e54f44b69a248e0ae26fb9d47f3b7ef893321724ea499eb633c1', '2014-09-12'),
	(33, 'test', '62582bdece2d1835529c4c949ab19c8eafd7be210b23ec4206c0e9cf4bbc6f2b03ba9570651ccf1e9d4972b1b9690fb5e594954eda5a1e6f3e90f198e1cd311146167ea35daf58ee', '2014-09-12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
