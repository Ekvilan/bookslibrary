package com.shribak.utils.reading;

import com.shribak.AbstractUnitTest;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class UnitTestBookReadingPDF extends AbstractUnitTest {
    private BookReaderPDF bookReaderPDF = new BookReaderPDF();

    @Test
    public void testReadFile() throws IOException {
        String path = "d:\\ProjectRepository\\BooksLibrary\\Education & Teaching\\servlets_tutorial.pdf";
        String expected = getFile("pdfTest.txt");

        String actual = bookReaderPDF.readFile(new File(path));

        assertNotNull(actual);
        assertEquals(expected, actual.substring(0, 500));
    }


    @Test(expected = NullPointerException.class)
    public void testReadFileNull() {
        bookReaderPDF.readFile(null);
    }
}
