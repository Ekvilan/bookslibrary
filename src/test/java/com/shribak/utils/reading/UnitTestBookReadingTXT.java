package com.shribak.utils.reading;

import com.shribak.AbstractUnitTest;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class UnitTestBookReadingTXT extends AbstractUnitTest{
    private BookReaderTXT bookReaderTXT = new BookReaderTXT();

    @Test
    public void testReadFile() throws IOException {
        String path = "d:/WEBProjects/SpringProjects/BooksLibrary_12_09/src/test/resources/com/shribak/utils/" +
                "reading/test1.txt";
        String expected = getFile("test1.txt");
        String actual = bookReaderTXT.readFile(new File(path));

        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    public void testReadFileIfFileEmpty() throws IOException {
        String path = "d:/WEBProjects/SpringProjects/BooksLibrary_12_09/src/test/resources/com/shribak/utils/" +
                "reading/empty.txt";
        String actual = bookReaderTXT.readFile(new File(path));

        assertTrue(actual.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testReadFileNull() {
        bookReaderTXT.readFile(null);
    }

}
