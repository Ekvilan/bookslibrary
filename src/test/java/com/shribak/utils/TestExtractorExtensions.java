package com.shribak.utils;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestExtractorExtensions {

    @Test
    public void testExtractExtensions() {
        assertEquals("txt", ExtractorExtensions.extractExtension("test.TXT"));
        assertEquals("doc", ExtractorExtensions.extractExtension("test.doc"));
        assertEquals("rtf", ExtractorExtensions.extractExtension("test.Rtf"));
        assertEquals("djvu", ExtractorExtensions.extractExtension("test.dJvU"));
        assertEquals("bf2", ExtractorExtensions.extractExtension("test.Bf2"));
    }
}
