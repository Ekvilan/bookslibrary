package com.shribak.dao;

import com.shribak.SpringTestConfiguration;
import com.shribak.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class TestUserDao {
    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @Test
    public void testGetUsers() {
        assertEquals(12, userService.getUsers().size());
    }

    @Test
    public void testFindById() {
        assertEquals("Admin", userService.getUserById(12));
    }
}
