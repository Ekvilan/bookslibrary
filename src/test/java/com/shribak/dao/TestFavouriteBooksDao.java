package com.shribak.dao;

import com.shribak.SpringTestConfiguration;
import com.shribak.entity.Book;
import com.shribak.service.FavouriteBookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import static org.junit.Assert.*;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class TestFavouriteBooksDao {
    @Autowired
    private FavouriteBookService favouriteBookService;

    @Test
    public void testGetFavouriteBooks() {
        List<Book> books = favouriteBookService.getFavouriteBooks(31);

        assertEquals(6, books.size());
    }
}
