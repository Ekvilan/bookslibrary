package com.shribak;


import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class AbstractUnitTest {
    public String getFile(String fileName) throws IOException {
        return IOUtils.toString(this.getClass().getResourceAsStream(fileName));
    }
}
