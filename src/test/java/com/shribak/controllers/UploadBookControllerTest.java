package com.shribak.controllers;

import com.shribak.SpringTestConfiguration;
import com.shribak.entity.Book;
import com.shribak.entity.User;
import com.shribak.service.BookService;
import com.shribak.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class UploadBookControllerTest {
    @Autowired
    private UserService userService;
    @Autowired
    private BookService bookService;

    @Test
    @Transactional
    public void testChangeUploadedBooksCount() {
        User user = userService.getUserByName("user");
        int oldCount = user.getUploadedBooksCount();
        bookService.save(new Book());
        user.setUploadedBooksCount(oldCount + 1);
        int newCount = user.getUploadedBooksCount();

        assertEquals(oldCount + 1, newCount);
    }

    @Test
    @Transactional
    public void testIfBookWasUploaded() {
        List<Book> oldListBooks = bookService.getAllBooks();
        bookService.save(new Book());
        List<Book> newListBooks = bookService.getAllBooks();

        assertEquals(oldListBooks.size() + 1, newListBooks.size());
    }
}
