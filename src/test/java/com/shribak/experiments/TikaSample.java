package com.shribak.experiments;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.parser.rtf.*;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class TikaSample {
    //private static final String filename = "d:\\Книги\\Учёба\\Programming\\SQL\\sql.pdf";
    //private static final String rtfname = "d:\\Книги\\Алекс Лесли - Новая ЖЖизнь без трусов.rtf";
    //private static final String rtfname = "d:\\Книги\\фэнтези\\ПЕРУМОВ\\Perumov_Voina_maga_Endshpil.rtf";
    //private static final String rtfname = "d:\\Книги\\фэнтези\\ПЕРУМОВ\\9- Война Мага (том 1).rtf";
    //private static final String rtfname = "d:\\Книги\\фэнтези\\ПЕРУМОВ\\5 - Алмазный Меч Деревянный Меч (том 1).rtf";
    private static final String rtfname = "d:\\Книги\\фэнтези\\ПЕРУМОВ\\10 - Война Мага (том 2).rtf";

    public TikaSample() throws IOException, SAXException, TikaException {
        //Logger log = Logger.getLogger(this.getClass().getName()); //create Logger
        /*Metadata metadata = new Metadata(); //create metadata
        Tika tika = new Tika(); //create tika
        InputStream is = getClass().getResourceAsStream(filename);
        File file = new File(filename);
        InputStream is2 = new FileInputStream(file);// create source InputStream
        ParseContext pc = new ParseContext(); //create xml-парсер
        ContentHandler ch = new BodyContentHandler(-1);//сдесь будет содержаться тело документа в виде plan text
        String mimeType = tika.detect(is);//определяем mimeType нашего документа
        System.out.println(mimeType);
        metadata.set(Metadata.CONTENT_TYPE, mimeType);//устанавливаем mimeType
        PDFParser parser = new PDFParser(); //create PDFParser
        //AutoDetectParser parser = new AutoDetectParser();//create универсальный парсер который на основе mimeType  принимает решение какой использовать узкоспециализированный парсер
        parser.parse(is2, ch, metadata, pc); //разбор файла на мелкие кусочки
        //System.out.println(ch.toString());
        *//*for (String name : metadata.names()) {//выводим метаданные документа
            System.out.println(name + ": " + metadata.get(name));
        }*//*
        if (is2 != null) is2.close();
        if (is != null) is.close();*/


        Metadata metadataRtf = new Metadata(); //create metadata
        Tika tikaRtf = new Tika(); //create tika
        InputStream isRtf = getClass().getResourceAsStream(rtfname);
        File fileRtf = new File(rtfname);
        InputStream is2Rtf = new FileInputStream(fileRtf);
        ParseContext pcRtf = new ParseContext();
        ContentHandler chRtf = new BodyContentHandler(-1);
        String mimeTypeRtf = tikaRtf.detect(isRtf);
        System.out.println(mimeTypeRtf);
        metadataRtf.set(Metadata.CONTENT_TYPE, mimeTypeRtf);
        RTFParser rtfParser = new RTFParser();
        //AutoDetectParser parser = new AutoDetectParser();//create универсальный парсер который на основе mimeType  принимает решение какой использовать узкоспециализированный парсер
        rtfParser.parse(is2Rtf, chRtf, metadataRtf, pcRtf); //разбор файла на мелкие кусочки

        System.out.println(chRtf.toString());
        for (String name : metadataRtf.names()) {//выводим метаданные документа
            System.out.println(name + ": " + metadataRtf.get(name));
        }
        if (is2Rtf != null) is2Rtf.close();
        if (isRtf != null) isRtf.close();


    }

    public static void main(String[] args) throws IOException, SAXException, TikaException {
        TikaSample tikaSample = new TikaSample();

    }


}
