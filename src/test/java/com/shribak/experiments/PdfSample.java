package com.shribak.experiments;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Dmitrii on 21.09.2014.
 */
public class PdfSample {

        private static final String filename = "d:\\Книги\\Учёба\\Programming\\JAVA\\hibernate_tutorial.pdf";

        public PdfSample() throws IOException, SAXException, TikaException {
            //Logger log = Logger.getLogger(this.getClass().getName()); //create Logger
            Metadata metadata = new Metadata(); //create metadata
            Tika tika = new Tika(); //create tika
            InputStream is = getClass().getResourceAsStream(filename);
            File file = new File(filename);
            InputStream is2 = new FileInputStream(file);// create source InputStream
            ParseContext pc = new ParseContext(); //create xml-парсер
            ContentHandler ch = new BodyContentHandler(-1);//сдесь будет содержаться тело документа в виде plan text
            String mimeType = tika.detect(is);//определяем mimeType нашего документа
            metadata.set(Metadata.CONTENT_TYPE, mimeType);//устанавливаем mimeType
            PDFParser parser = new PDFParser(); //create PDFParser
            //AutoDetectParser parser = new AutoDetectParser();//create универсальный парсер который на основе mimeType  принимает решение какой использовать узкоспециализированный парсер
            parser.parse(is2, ch, metadata, pc); //разбор файла на мелкие кусочки
            //log.info(ch.toString());//выводим текст документа
            System.out.println(ch.toString());
            /*for (String name : metadata.names()) {//выводим метаданные документа
                log.info(name + ": " + metadata.get(name));
            }*/
            if (is2 != null) is2.close();
            if (is != null) is.close();
        }

        public static void main(String[] args) throws IOException, SAXException, TikaException {
            PdfSample tikaSample = new PdfSample();
        }

}
