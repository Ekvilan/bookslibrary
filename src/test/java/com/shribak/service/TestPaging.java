package com.shribak.service;

import com.shribak.SpringTestConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class TestPaging {
    @Autowired
    Paging paging;

    List<String> items;

    @Before
    public void init() {
        items = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
                "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32");
    }

    @Test
    public void testSetPaging() {
        paging.setPaging(items);

        assertEquals(1, paging.getCurrentPage());
        assertEquals(4, paging.getPageCount());
    }

    @Test
    public void testPageUp() {
        paging.setPaging(items);
        List<String> actual = paging.pageUp();

        assertEquals(2, paging.getCurrentPage());
        assertEquals(10, actual.size());
        assertEquals("11", actual.get(0));
        assertEquals("20", actual.get(9));
    }

    @Test
    public void testPageDown() {
        paging.setPaging(items);
        paging.pageUp();
        paging.pageUp();
        paging.pageUp();
        List<String> actual = paging.pageDown();

        assertEquals(3, paging.getCurrentPage());
        assertEquals(10, actual.size());
        assertEquals("27", actual.get(6));
    }

    @Test
    public void testLastPage() {
        paging.setPaging(items);
        List<String> actual = paging.lastPage();

        assertEquals(4, paging.getCurrentPage());
        assertEquals(2, actual.size());
    }

    @Test
    public void testFirstPage() {
        paging.setPaging(items);
        paging.lastPage();
        List<String> actual = paging.firstPage();

        assertEquals(1, paging.getCurrentPage());
        assertEquals(10, actual.size());
    }
}
