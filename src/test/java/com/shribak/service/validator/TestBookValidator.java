package com.shribak.service.validator;

import com.shribak.SpringTestConfiguration;
import com.shribak.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BindingResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class TestBookValidator {
    @Autowired
    private BookValidator bookValidator;

    @Test
    public void testEmptyBookAndEmptyFields() {
        Book book = new Book();
        //bookValidator.validate(book, new BindingResult());
    }
}
