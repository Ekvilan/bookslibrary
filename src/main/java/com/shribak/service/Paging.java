package com.shribak.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Paging<E> {
    private static final int ITEMS_PER_PAGE = 10;
    private int pageCount = 0;
    private int remain;
    private int currentPage;
    private List<E> listItems;
    private List<E> onePage;

    public void setPaging(List<E> items) {
        listItems = items;
        onePage = new ArrayList<E>();

        if(!listItems.isEmpty()) {
            calculatePageCount();
        }
        else {
            pageCount = 0;
        }
    }

    private void calculatePageCount() {
        remain = 0;
        currentPage = 1;

        if(listItems.size() < ITEMS_PER_PAGE) {
            pageCount = 1;
            remain = listItems.size();
        } else
            if(listItems.size() == ITEMS_PER_PAGE) {
                pageCount = 1;
            } else
                if ((listItems.size() > ITEMS_PER_PAGE) && (listItems.size() % ITEMS_PER_PAGE == 0)) {
                    pageCount = listItems.size() / ITEMS_PER_PAGE;
                } else {
                    pageCount = listItems.size() / ITEMS_PER_PAGE + 1;
                    remain = listItems.size() % ITEMS_PER_PAGE;
                }
    }

    public List<E> firstPage() {
        int realSize = pageCount > 1 ? ITEMS_PER_PAGE : listItems.size();
        onePage = listItems.subList(0, realSize);
        currentPage = 1;
        return onePage;
    }

    public List<E> lastPage() {
        onePage = listItems.subList(ITEMS_PER_PAGE * (pageCount - 1), listItems.size());
        currentPage = pageCount;
        return onePage;
    }

    public List<E> pageUp() {
        if(currentPage < pageCount) {
            currentPage++;
        }
        selectItems();
        return onePage;
    }

    public List<E> pageDown() {
        if(currentPage > 1) {
            currentPage--;
        }
        selectItems();
        return onePage;
    }

    private void selectItems () {
        int realSize = remain == 0 ? ITEMS_PER_PAGE * currentPage : listItems.size();

        if(currentPage == pageCount) {
            onePage = listItems.subList(ITEMS_PER_PAGE * (currentPage - 1), realSize);
        }
        else if(currentPage < pageCount){
            onePage = listItems.subList(ITEMS_PER_PAGE * (currentPage - 1), (ITEMS_PER_PAGE * (currentPage - 1)) + ITEMS_PER_PAGE);
        }
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public int getPageCount() {
        return pageCount;
    }
}
