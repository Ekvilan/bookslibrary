package com.shribak.service.validator;

import com.shribak.entity.User;
import com.shribak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class RegisterValidator implements Validator {
    @Autowired
    private UserService userService;

    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }


    public void validate(Object target, Errors errors) {
        User user = (User)target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.empty", "Password must not be empty.");
        if(!(user.getPassword()).equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "confirmPassword.passwordDontMatch", "Password don't match");
        }

        User dbUser = userService.getUserByName(user.getUserName());
        if(dbUser != null) {
            errors.rejectValue("userName", "userName.using", "This name already used");
        }
    }
}
