package com.shribak.service.validator;

import com.shribak.entity.Book;
import com.shribak.service.BookService;
import com.shribak.utils.ExtractorExtensions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

@Service
public class BookValidator implements Validator {
    @Autowired
    private BookService bookService;

    private List<String> extensions = Arrays.asList("txt", "fb2", "rtf");

    @Override
    public boolean supports(Class<?> clazz) {
        return Book.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object book, Errors errors) {
        Book file = (Book)book;
        String ext = ExtractorExtensions.extractExtension(file.getFile().getOriginalFilename());

        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty", "Book's name must not be empty.");
        ValidationUtils.rejectIfEmpty(errors, "author", "author.empty", "Author must not be empty.");

        if(file.getFile().getSize() == 0) {
            errors.rejectValue("file", "file.empty", "Please select a file");
        }

        if(!extensions.contains(ext) && file.getFile().getSize() > 0) {
            errors.rejectValue("file", "extension.wrong", "You can upload only txt and fb2 format files.");
        }
        Book dbBook = bookService.getBookByName(file.getName());
        if(dbBook != null) {
            errors.rejectValue("name", "name.exist", "This book already exist in the database.");
        }
    }
}
