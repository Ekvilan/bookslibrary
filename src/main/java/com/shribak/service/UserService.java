package com.shribak.service;

import com.shribak.entity.User;

import java.util.List;

public interface UserService {
    public void save(User user);
    public List<User> getUsers();
    public void deleteUser(Integer id);
    public User getUserById(Integer id);
    public User getUserByNameAndPassword(String name, String password);
    public User getUserByName(String name);
    public List<User> getTopUploader();
    public void update(User user);
}
