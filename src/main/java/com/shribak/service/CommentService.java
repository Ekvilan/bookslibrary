package com.shribak.service;


import com.shribak.entity.Comment;


public interface CommentService {
    public void save(Comment comment);
}
