package com.shribak.service;

import com.shribak.entity.Book;
import com.shribak.entity.FavouriteBook;

import java.util.List;

public interface FavouriteBookService {
    public void add(FavouriteBook book);
    public void delete(Integer id);
    public List<Book> getFavouriteBooks(Integer userId);
}
