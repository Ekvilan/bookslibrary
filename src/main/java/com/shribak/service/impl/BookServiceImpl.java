package com.shribak.service.impl;

import com.shribak.dao.BookDao;
import com.shribak.entity.Book;
import com.shribak.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;

    public void save(Book book) {
        bookDao.save(book);
    }

    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    public Book getBookById(Integer id) {
        return bookDao.getBookById(id);
    }

    public Book getBookByName(String name) {
        return bookDao.getBookByName(name);
    }

    public List<Book> getBookByTextField(String name) {
        return bookDao.getBookByTextField(name);
    }

    public List<Book> getBookByLetter(String letter) {
        return bookDao.getBookByLetter(letter);
    }

    public void deleteBook(Integer id) {
        bookDao.deleteBook(id);
    }

    public void update(Book book) {
        bookDao.update(book);
    }

    public List<Book> getMostDownloadedBooks() {
        return bookDao.getMostDownloadedBooks();
    }

    public List<Book> getMostReadBooks() {
        return bookDao.getMostReadBooks();
    }

    public List<Book> getLastUploadedBooks() {
        return bookDao.getLastUploadedBooks();
    }
}
