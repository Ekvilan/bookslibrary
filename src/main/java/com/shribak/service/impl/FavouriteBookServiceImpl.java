package com.shribak.service.impl;


import com.shribak.dao.FavouriteBookDao;
import com.shribak.entity.Book;
import com.shribak.entity.FavouriteBook;
import com.shribak.service.FavouriteBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FavouriteBookServiceImpl implements FavouriteBookService {
    @Autowired
    private FavouriteBookDao favouriteBookDao;

    @Override
    public void add(FavouriteBook book) {
        favouriteBookDao.add(book);
    }

    @Override
    public void delete(Integer id) {
        favouriteBookDao.delete(id);
    }

    @Override
    public List<Book> getFavouriteBooks(Integer userId) {
        return favouriteBookDao.getFavouriteBooks(userId);
    }
}
