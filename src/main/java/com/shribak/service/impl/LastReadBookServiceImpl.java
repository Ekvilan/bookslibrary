package com.shribak.service.impl;

import com.shribak.dao.LastReadBookDao;
import com.shribak.entity.Book;
import com.shribak.entity.LastReadBook;
import com.shribak.service.LastReadBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LastReadBookServiceImpl implements LastReadBookService {
    @Autowired
    private LastReadBookDao lastReadBookDao;

    @Override
    public void add(LastReadBook book) {
        lastReadBookDao.add(book);
    }

    @Override
    public List<Book> getLastReadBooks(Integer userId) {
        return lastReadBookDao.getLastReadBooks(userId);
    }

    @Override
    public LastReadBook getBook(Integer userId, Integer bookId) {
        return lastReadBookDao.getBook(userId, bookId);
    }

    @Override
    public void update(LastReadBook book) {
        lastReadBookDao.update(book);
    }
}
