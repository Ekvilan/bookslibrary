package com.shribak.service;


import com.shribak.entity.Book;
import com.shribak.entity.LastReadBook;

import java.util.List;

public interface LastReadBookService {
    public void add(LastReadBook book);
    public List<Book> getLastReadBooks(Integer userId);
    public LastReadBook getBook(Integer userId, Integer bookId);
    public void update(LastReadBook book);
}
