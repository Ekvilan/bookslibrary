package com.shribak.utils.reading;

import java.io.File;
import java.util.List;

public interface BookReader {
    public List<String> getPages(File file);
}
