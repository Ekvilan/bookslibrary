package com.shribak.utils.reading;


import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.rtf.RTFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.List;

public class BookReaderRTF extends BookReaderDefault {
    private static Logger logger = LoggerFactory.getLogger(BookReaderRTF.class);
    private String rtfRegExp = "\n";

    public List<String> getPages(File file) {
        return createPage(readFile(file));
    }

    @Override
    public String readFile(File file) {
        Metadata metadata = new Metadata();
        Tika tika = new Tika();
        ParseContext parseContext = new ParseContext();
        ContentHandler contentHandler = new BodyContentHandler(-1);
        RTFParser rtfParser = new RTFParser();

        String fileName = file.getAbsolutePath();
        InputStream isMain = getClass().getResourceAsStream(fileName);
        InputStream isRTF = null;

        try {
            File fileRtf = new File(fileName);
            isRTF = new FileInputStream(fileRtf);

            String mimeType = tika.detect(isMain);
            metadata.set(Metadata.CONTENT_TYPE, mimeType);
            rtfParser.parse(isRTF, contentHandler, metadata, parseContext);

            if (isRTF != null) {
                isRTF.close();
            }
            if(isMain != null) {
                isMain.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            logger.info("can't parse " + fileName + ":\n" + e.getMessage());
        }
        catch (TikaException e) {
            e.printStackTrace();
        }

        return contentHandler.toString();
    }

    @Override
    public List<String>createPage(String content) {
        String book = decorateText(content, rtfRegExp);
        return divideIntoPages(book);
    }
}
