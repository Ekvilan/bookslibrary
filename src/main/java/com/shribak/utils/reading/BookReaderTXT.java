package com.shribak.utils.reading;


import java.io.File;
import java.util.List;


public class BookReaderTXT extends BookReaderDefault{
    private String txtRegExp = "\r";

    public List<String> getPages(File file) {
        return createPage(readFile(file));
    }

    @Override
    public List<String>createPage(String content) {
        String book = decorateText(content, txtRegExp);
        return divideIntoPages(book);
    }
}
