package com.shribak.utils.reading;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class BookReaderDoc extends BookReaderDefault {
    private String docRegExp = "\n";

    public List<String> getPages(File file) {
        return createPage(readFile(file));
    }

    @Override
    public String readFile(File file) {
        Metadata metadata = new Metadata();
        Tika tika = new Tika();
        ParseContext parseContext = new ParseContext();
        ContentHandler contentHandler = new BodyContentHandler(-1);
        AutoDetectParser parser = new AutoDetectParser();

        String fileName = file.getAbsolutePath();
        InputStream isMain = getClass().getResourceAsStream(fileName);
        InputStream isDoc = null;

        try {
            File fileRtf = new File(fileName);
            isDoc = new FileInputStream(fileRtf);

            String mimeType = tika.detect(isMain);
            metadata.set(Metadata.CONTENT_TYPE, mimeType);
            parser.parse(isDoc, contentHandler, metadata, parseContext);

            if (isDoc != null) {
                isDoc.close();
            }
            if(isMain != null) {
                isMain.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            e.printStackTrace();
        }
        catch (TikaException e) {
            e.printStackTrace();
        }

        return contentHandler.toString();
    }

    @Override
    public List<String>createPage(String content) {
        String book = decorateText(content, docRegExp);
        return divideIntoPages(book);
    }
}
