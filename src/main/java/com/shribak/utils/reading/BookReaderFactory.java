package com.shribak.utils.reading;

import org.springframework.stereotype.Component;

@Component
public class BookReaderFactory {
    private BookReader bookReader;

    public BookReader createPageBuilder(String type) {
        if("doc".equals(type)) {
            bookReader = new BookReaderDoc();
        }
        else if("rtf".equals(type)) {
            bookReader = new BookReaderRTF();
        }
        else if ("pdf".equals(type)){
            bookReader = new BookReaderPDF();
        }
        else if ("fb2".equals(type)){
            bookReader = new BookReaderFB2();
        }
        else {
            bookReader = new BookReaderTXT();
        }
        return bookReader;
    }
}
