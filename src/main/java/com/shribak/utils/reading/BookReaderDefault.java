package com.shribak.utils.reading;


import com.shribak.utils.EncodingDetector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.join;

public abstract class BookReaderDefault implements BookReader{

    public abstract List<String> getPages(File file);
    public abstract List<String> createPage(String content);

    public String readFile(File file) {
        byte[] buffer = null;
        FileInputStream inputStream = null;
        String encoding = "";

        try {
            inputStream = new FileInputStream(file);
            buffer = new byte[inputStream.available()];

            inputStream.read(buffer);
            encoding = EncodingDetector.checkEncoding(buffer);

            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            text = new String(buffer, encoding);
            text = new String(text.getBytes("UTF-8"), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public String decorateText(String content, String regexp) {
        List<String> lines = new ArrayList<String>();
        int lettersToLine = 60;

        String[] str = content.split(regexp);

        for(int i = 0; i < str.length - 1; i++) {
            if(str[i].length() > 1) {
                if(str[i].endsWith(".") || str[i].endsWith("!") || str[i].endsWith("?")) {
                    lines.add(str[i] + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                }
                else {
                    if(str[i].length() < lettersToLine - 10) {
                        lines.add(str[i] + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    }
                    else {
                        lines.add(str[i] + " ");
                    }
                }
            }
            else {
                lines.add("<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            }
        }
        return join(lines, "");
    }

    public List<String> divideIntoPages(String book) {
        List<String> pages = new ArrayList<String>();
        int size = 4096;
        int start = 0;
        int iterations = book.length() / size;
        int remainder = book.length() % size;

        for(int i = 0; i < iterations; i++) {
            String pageContent = book.substring(start, start + size);
            if(pageContent.endsWith(" ")) {
                pages.add(pageContent);
                start += size;
            }
            else {
                int index = pageContent.lastIndexOf(" ");
                pageContent = pageContent.substring(0, index + 1);
                pages.add(pageContent);
                start += pageContent.length();
            }
        }
        pages.add(book.substring(start, start + remainder));
        return pages;
    }
}
