package com.shribak.utils.reading;


import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import java.io.*;
import java.util.List;


public class BookReaderPDF extends BookReaderDefault {
    private String pdfRegExp = "\n";

    public List<String> getPages(File file) {
        return createPage(readFile(file));
    }

    @Override
    public String readFile(File file) {
        PdfReader reader = null;
        String text = "";
        TextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

        try {
            reader = new PdfReader(file.getAbsolutePath());
            int size = reader.getNumberOfPages();

            for(int i = 1; i <= size; i++) {
                text = PdfTextExtractor.getTextFromPage(reader, i, strategy);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

    @Override
    public List<String>createPage(String content) {
        String book = decorateText(content, pdfRegExp);
        return divideIntoPages(book);
    }
}
