package com.shribak.utils.reading;

import java.io.File;
import java.util.List;

public class BookReaderFB2 extends BookReaderDefault{
    private String fb2RegExp = "</p>";

    public List<String> getPages(File file) {
        return createPage(readFile(file));
    }

    @Override
    public List<String>createPage(String content) {
        String book = decorateText(content, fb2RegExp);
        return divideIntoPages(book);
    }

    @Override
    public String decorateText(String content, String regexp) {
        String newContent = content.replaceAll("<image.+\\/>", "");
        newContent = newContent.replaceAll("<p>", "");
        newContent = newContent.replaceAll(regexp, "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        return newContent;
    }
}
