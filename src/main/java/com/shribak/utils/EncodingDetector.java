package com.shribak.utils;

import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class EncodingDetector {
    private static final Logger logger = LoggerFactory.getLogger(EncodingDetector.class);

    public static String checkEncoding(byte[] buffer) {
        UniversalDetector detector = new UniversalDetector(null);
        detector.handleData(buffer, 0, buffer.length - 1);

        String encoding = detector.getDetectedCharset();
        detector.reset();

        if (encoding == null) {
            logger.info("Not detected encoding");
            encoding = "UTF-8";
        }
        return encoding;
    }
}
