package com.shribak.utils;


public class ExtractorExtensions {

    public static String extractExtension(String fileName) {
        String[] words = fileName.toString().split("\\.");
        return words[words.length - 1].toLowerCase();
    }
}
