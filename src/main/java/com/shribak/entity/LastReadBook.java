package com.shribak.entity;

import javax.persistence.*;

@Entity
@Table(name = "lastreadbooks")
public class LastReadBook {
    public final static int SIZE_LAST_READ_BOOKS = 5;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "page")
    private int page = 1;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    public LastReadBook() {}

    public LastReadBook(User user, Book book) {
        this.user = user;
        this.book = book;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LastReadBook)) return false;

        LastReadBook that = (LastReadBook) o;

        if (book != null ? !book.equals(that.book) : that.book != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (book != null ? book.hashCode() : 0);
        return result;
    }
}
