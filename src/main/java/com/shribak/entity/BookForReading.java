package com.shribak.entity;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.util.List;

@Component
public class BookForReading {
    private List<String> bookPages;
    private int pages;

    public String getPage(int number) {
        return bookPages.get(number);
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<String> getBookPages() {
        return bookPages;
    }

    public void setBookPages(List<String> bookPages) {
        this.bookPages = bookPages;
    }
}
