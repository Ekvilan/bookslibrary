package com.shribak.entity;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {
	@Id
    @GeneratedValue
	private Integer id;
	
	@OneToOne
	private User user;

	private Integer role;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getRole() {
		return role;
	}
	public void setRole(Integer role) {
		this.role = role;
	}
}
