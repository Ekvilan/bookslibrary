package com.shribak.controller.user;

import com.shribak.entity.Role;
import com.shribak.entity.User;
import com.shribak.service.UserService;
import com.shribak.service.security.PasswordEncoder;
import com.shribak.service.validator.RegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.GET)
    public String register(ModelMap model) {
        User user = new User();
        model.put("user", user);
        return "register";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processRegister(User user, BindingResult result, Model model) {
        registerValidator.validate(user, result);
        if (result.hasErrors()) {
            return "register";
        }

        String password = user.getPassword();
        String encodedPassword = passwordEncoder.encodePassword(password, "password");
        user.setPassword(encodedPassword);

        model.addAttribute("pass", password);

        Role role = new Role();
        role.setUser(user);

        if(user.getUserName().startsWith("admin")) {
            role.setRole(1);
        }
        else {
            role.setRole(2);
        }

        user.setRole(role);
        user.setRegistrationDate(new Date());
        userService.save(user);

        return "welcome";
    }
}
