package com.shribak.controller.user;

import com.shribak.entity.Book;
import com.shribak.service.BookService;
import com.shribak.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;


@Controller
public class AdminController {
    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/showUsers/delete/{userId}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("userId") Integer userId) {
        userService.deleteUser(userId);
        return "redirect:/showUsers";
    }

    @RequestMapping(value = "/showBooks/delete/{bookId}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable("bookId") Integer bookId) {
        Book book = bookService.getBookById(bookId);
        File file = new File(System.getenv("OPENSHIFT_DATA_DIR") + book.getRealName());
        if(file.delete()) {
            logger.info("Delete" + book.getName() + "is failed. ");
        }

        bookService.deleteBook(bookId);
        return "redirect:/showBooks";
    }


}
