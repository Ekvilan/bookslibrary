package com.shribak.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model, String message) {
        model.addAttribute("message", message);
        return "login";
    }

    @RequestMapping(value = "/login/failure")
    public String loginFailure() {
        String message = "Login Failed!";
        return "redirect:/login?message="+message;
    }
}
