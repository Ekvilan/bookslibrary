package com.shribak.controller.user;

import com.shribak.entity.User;
import com.shribak.service.Paging;
import com.shribak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ShowUsersController {
    @Autowired
    private UserService userService;

    @Autowired
    private Paging paging;

    @RequestMapping(value = "/showUsers", method = RequestMethod.GET)
    public String showAllBooks(Model model) {
        List<User> users = userService.getUsers();
        paging.setPaging(users);

        List<User> onePage = paging.firstPage();

        model.addAttribute("users", onePage);
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());

        if(!onePage.isEmpty()) {
            return "showUsers";
        }
        else {
            model.addAttribute("attribute", "user");
            return "/searchFail";
        }
    }

    @RequestMapping(value = "/showUsers/pageUP", method = RequestMethod.GET)
    public String pageUp(Model model) {
        model.addAttribute("users", paging.pageUp());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "showUsers";
    }

    @RequestMapping(value = "/showUsers/pageDown", method = RequestMethod.GET)
    public String pageDown(Model model) {
        model.addAttribute("users", paging.pageDown());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "showUsers";
    }

    @RequestMapping(value = "/showUsers/firstPage", method = RequestMethod.GET)
    public String firstPage(Model model) {
        model.addAttribute("users", paging.firstPage());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "showUsers";
    }

    @RequestMapping(value = "/showUsers/lastPage", method = RequestMethod.GET)
    public String lastPage(Model model) {
        model.addAttribute("users", paging.lastPage());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "showUsers";
    }
}
