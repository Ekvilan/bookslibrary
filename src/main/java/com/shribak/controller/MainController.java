package com.shribak.controller;

import com.shribak.entity.Book;
import com.shribak.entity.User;
import com.shribak.service.BookService;
import com.shribak.service.LastReadBookService;
import com.shribak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class MainController {
    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;
    @Autowired
    private LastReadBookService lastReadBookService;

    @RequestMapping("/")
    public String index(Model model) {
        List<Book> downloadedBooks = bookService.getMostDownloadedBooks();
        List<Book> readBooks = bookService.getMostReadBooks();

        model.addAttribute("downloadedBooks", downloadedBooks);
        model.addAttribute("readBooks", readBooks);

        return "index";
    }

    @RequestMapping("/welcome")
    public String getWelcomePage() {
        return "welcome";
    }

    @RequestMapping("/main")
    public String getMainPage(Model model) {
        User user = userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName());
        List<User> topUploader = userService.getTopUploader();
        List<Book> lastUploadedBooks = bookService.getLastUploadedBooks();
        List<Book> lastReadBooks = lastReadBookService.getLastReadBooks(user.getId());

        model.addAttribute("topUploader", topUploader);
        model.addAttribute("lastUploadedBooks", lastUploadedBooks);
        model.addAttribute("lastReadBooks", lastReadBooks);

        return "main";
    }
}
