package com.shribak.controller.book;


import com.shribak.entity.*;
import com.shribak.service.BookService;
import com.shribak.service.FavouriteBookService;
import com.shribak.service.LastReadBookService;
import com.shribak.service.UserService;
import com.shribak.utils.ExtractorExtensions;
import com.shribak.utils.reading.BookReader;
import com.shribak.utils.reading.BookReaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;
import java.util.List;

@Controller
public class ReadBookController {
    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private FavouriteBookService favouriteBookService;

    @Autowired
    private LastReadBookService lastReadBookService;

    @Autowired
    private BookForReading bookForReading;

    @Autowired
    private BookReaderFactory bookReaderFactory;

    private int page = 1;
    private Book book;

    @RequestMapping("/read")
    public String read() {
        return "read";
    }

    @RequestMapping(value = "/read/{bookId}", method = RequestMethod.GET)
    public String doRead(@PathVariable("bookId") Integer bookId, Model model) throws IOException {
        book = bookService.getBookById(bookId);
        String path = System.getenv("OPENSHIFT_DATA_DIR") + book.getRealName();
        File downloadedFile = new File(path);
        String type = ExtractorExtensions.extractExtension(downloadedFile.toString());
        BookReader bookReader = bookReaderFactory.createPageBuilder(type);
        List<String> bookPages = bookReader.getPages(downloadedFile);

        bookForReading.setBookPages(bookPages);
        bookForReading.setPages(bookPages.size());

        LastReadBook lastReadBook = lastReadBookService.getBook(getUser().getId(), book.getId());
        page = lastReadBook != null ? lastReadBook.getPage() : 1;

        setModel(model);

        book.setReadCount(book.getReadCount() + 1);
        bookService.update(book);

        saveBookToLastReadBooks();

        return "read";
    }

    @RequestMapping(value = "/nextBookPage", method = RequestMethod.GET)
    public String nextPage(Model model) {
        if(page < bookForReading.getPages()) {
            page++;
        }
        setModel(model);
        return "read";
    }

    @RequestMapping(value = "/lastBookPage", method = RequestMethod.GET)
    public String lastPage(Model model) {
        if(page > 1) {
            page--;
        }
        setModel(model);
        return "read";
    }

    @RequestMapping(value = "/addFavouriteBook", method = RequestMethod.GET)
    public String addBookToFavourite(Model model) {
        FavouriteBook favouriteBook = new FavouriteBook(getUser(), book);
        favouriteBookService.add(favouriteBook);

        setModel(model);
        return "read";
    }

    private void setModel(Model model) {
        model.addAttribute("bookPart", bookForReading.getPage(page-1));
        model.addAttribute("page", page);
        model.addAttribute("pageCount", bookForReading.getPages());
    }

    private void saveBookToLastReadBooks() {
        LastReadBook lastReadBook = new LastReadBook(getUser(), book);

        lastReadBookService.add(lastReadBook);
    }

    @RequestMapping(value = "/savePage", method = RequestMethod.GET)
    public String savePage(Model model) {
        LastReadBook lastReadBook;
        if(book != null) {
            lastReadBook = lastReadBookService.getBook(getUser().getId(), book.getId());
            lastReadBook.setPage(page);
            lastReadBookService.update(lastReadBook);
        }
        setModel(model);

        return "read";
    }

    private User getUser() {
        return userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName());
    }
}
