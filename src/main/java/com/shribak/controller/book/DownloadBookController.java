package com.shribak.controller.book;

import com.shribak.entity.Book;
import com.shribak.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
public class DownloadBookController {
    private static final int BUFFER_SIZE = 4096;

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/download/{bookId}", method = RequestMethod.GET)
    public void doDownload(@PathVariable("bookId") Integer bookId, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        ServletContext context = request.getServletContext();

        Book book = bookService.getBookById(bookId);
        String path = System.getenv("OPENSHIFT_DATA_DIR") + book.getRealName();

        File downloadedFile = new File(path);
        FileInputStream inputStream = new FileInputStream(downloadedFile);

        String mimeType = context.getMimeType(path);
        if(mimeType == null) {
            mimeType = "application/octet-stream";
        }
        response.setContentType(mimeType);
        response.setContentLength((int) downloadedFile.length());

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadedFile.getName());
        response.setHeader(headerKey, headerValue);

        OutputStream outputStream = response.getOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;

        while((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        inputStream.close();
        outputStream.flush();
        outputStream.close();

        book.setDownloadCount(book.getDownloadCount() + 1);
        bookService.update(book);
    }
}
