package com.shribak.controller.book;

import com.shribak.entity.Book;

import com.shribak.entity.User;
import com.shribak.service.BookService;
import com.shribak.service.UserService;
import com.shribak.service.validator.BookValidator;
import com.shribak.utils.ExtractorExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

@Controller
public class UploadBookController {
    private static final Logger logger = LoggerFactory.getLogger(UploadBookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private BookValidator bookValidator;

    @RequestMapping(value="/upload", method= RequestMethod.GET)
    public String provideUploadInfo() {
        return "upload";
    }

    @RequestMapping(value = "/saveBook", method = RequestMethod.POST)
    public String upload( @ModelAttribute("book") Book book, BindingResult result, Model map,
                          HttpServletRequest request) throws ServletException {

        bookValidator.validate(book, result);
        if(result.hasErrors()) {
            return "upload";
        }

        Enumeration<String> list = request.getParameterNames();
        List<String> params = new ArrayList<String>();

        while(list.hasMoreElements()) {
            String param = request.getParameter(list.nextElement());
            params.add(param);
        }

        String name = book.getFile().getOriginalFilename();
        String category = params.get(1);

        if(category.toLowerCase().equals("0")) {
            category = "Other";
        }

        map.addAttribute("file", name);

        try {
            byte[] bytes = book.getFile().getBytes();
            String path = System.getenv("OPENSHIFT_DATA_DIR") + name;
            File serverFile = new File(path);

            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

        } catch (IOException e) {
            logger.info("You failed to upload " + name+ "to the server " + e.getMessage());
        }

        book.setName(params.get(2));
        book.setAuthor(params.get(0));
        book.setCategory(category);
        book.setRealName(name);
        book.setUploadDate(new Date());
        book.setExtension(ExtractorExtensions.extractExtension(name).toLowerCase());

        User user = userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName());
        book.setUser(user);
        bookService.save(book);

        user.setUploadedBooksCount(user.getUploadedBooksCount() + 1);
        userService.update(user);

        return "mainUpload";
    }
}
