package com.shribak.controller.book;

import com.shribak.entity.Book;
import com.shribak.entity.Comment;
import com.shribak.service.BookService;
import com.shribak.service.CommentService;
import com.shribak.service.UserService;
import com.shribak.utils.ExtractorExtensions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class AboutBookController {
    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService  commentService;

    private Book book;
    private List<Comment> comments = new ArrayList<Comment>();

    @RequestMapping(value = "/aboutBook{bookId}", method = RequestMethod.GET)
    public String showAboutBookPage(@PathVariable("bookId") Integer bookId, Model model) {
        Book book = bookService.getBookById(bookId);
        this.book = book;
        comments = book.getComments();

        model.addAttribute("comments", comments);
        model.addAttribute("book", book);
        return "aboutBook";
    }

    @RequestMapping(value = "/image")
    public void getImage(HttpServletResponse response) throws FileNotFoundException {
        File file = null;
        try {
            String imagePath = book.getImage();
            file = new File(imagePath);
        } catch (NullPointerException e) {
            file = org.springframework.util.ResourceUtils.getFile("classpath:images/empty_book.jpg");
        }

        FileInputStream inputStream = new FileInputStream(file);
        response.setContentType("image/jpeg");
        try {
            byte[] image = new byte[inputStream.available()];
            inputStream.read(image);
            response.getOutputStream().write(image);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/aboutBook/addComment", method = RequestMethod.POST)
    public String addComment(@ModelAttribute("comment") Comment comment, Model model) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        comment.setDate(sdf.format(new Date()));
        comment.setBook(book);
        comment.setUser(userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName()));

        commentService.save(comment);
        comments.add(comment);
        model.addAttribute("comments", comments);

        return "redirect:/aboutBook" + book.getId();
    }

    @RequestMapping(value = "/saveImage", method = RequestMethod.POST)
    public String uploadImage(@ModelAttribute("image") MultipartFile file) {
        String path = System.getenv("OPENSHIFT_DATA_DIR") + "b" + book.getId() + "." +
                ExtractorExtensions.extractExtension(file.getOriginalFilename());
        File image = new File(path);

        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(image));
            byte[] bytes = file.getBytes();
            stream.write(bytes);
            stream.close();
        } catch (IOException e) {e.printStackTrace();}

        book.setImage(image.getAbsolutePath());
        bookService.update(book);

        return "redirect:/aboutBook" + book.getId();
    }

    @RequestMapping(value = "/aboutBook/addDescription", method = RequestMethod.POST)
    public String addDescription(@ModelAttribute("description") String description, Model model) {
        book.setDescription(description);
        bookService.update(book);
        model.addAttribute("description", description);

        return "redirect:/aboutBook" + book.getId();
    }
}
