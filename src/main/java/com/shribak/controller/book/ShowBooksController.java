package com.shribak.controller.book;

import com.shribak.entity.Book;
import com.shribak.entity.User;
import com.shribak.service.BookService;
import com.shribak.service.FavouriteBookService;
import com.shribak.service.Paging;
import com.shribak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ShowBooksController {
    @Autowired
    private BookService bookService;

    @Autowired
    private FavouriteBookService favouriteBookService;

    @Autowired
    private UserService userService;

    @Autowired
    private Paging paging;

    @RequestMapping(value = "/showBooks", method = RequestMethod.GET)
    public String showAllBooks(Model model) {
        List<Book> books = bookService.getAllBooks();
        paging.setPaging(books);

        List<Book> onePage = paging.firstPage();
        setModel(model, onePage);

        if(!onePage.isEmpty()) {
            return "showBooks";
        }
        else {
            model.addAttribute("attribute", "book");
            return "/searchFail";
        }
    }

    @RequestMapping(value = "/showFavouriteBooks", method = RequestMethod.GET)
    public String showFavouriteBooks(Model model) {
        User user = userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Book> books = favouriteBookService.getFavouriteBooks(user.getId());
        paging.setPaging(books);

        List<Book> onePage = paging.firstPage();
        setModel(model, onePage);

        if(!onePage.isEmpty()) {
            return "showFavouriteBooks";
        }
        else {
            model.addAttribute("attribute", "book");
            return "/searchFail";
        }
    }

    private void setModel(Model model, List<Book> onePage) {
        model.addAttribute("books", onePage);
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
    }

    @RequestMapping(value = "/showBooks/pageUP", method = RequestMethod.GET)
    public String pageUp(Model model) {
        model.addAttribute("books", paging.pageUp());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "/showBooks";
    }

    @RequestMapping(value = "/showBooks/pageDown", method = RequestMethod.GET)
        public String pageDown(Model model) {
        model.addAttribute("books", paging.pageDown());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "/showBooks";
    }

    @RequestMapping(value = "/showBooks/firstPage", method = RequestMethod.GET)
    public String firstPage(Model model) {
        model.addAttribute("books", paging.firstPage());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "/showBooks";
    }

    @RequestMapping(value = "/showBooks/lastPage", method = RequestMethod.GET)
    public String lastPage(Model model) {
        model.addAttribute("books", paging.lastPage());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());
        return "/showBooks";
    }

    @RequestMapping(value = "/searchBookByLetter", method = RequestMethod.GET)
    public String searchByLetter(Model model, HttpServletRequest request) {
        String letter = request.getParameter("letter");

        List<Book> books = bookService.getBookByLetter(letter.toLowerCase());
        paging.setPaging(books);

        model.addAttribute("books", paging.firstPage());
        model.addAttribute("page", paging.getCurrentPage());
        model.addAttribute("pageCount", paging.getPageCount());

        if(!books.isEmpty()) {
            return "showBooks";
        }
        else {
            model.addAttribute("attribute", "book");
            return "searchFail";
        }
    }

    @RequestMapping(value = "/searchBookByTextField", method = RequestMethod.POST)
    public String searchByTextField(Model map, HttpServletRequest request) {
        String searchName = request.getParameter("searchBook");

        List<Book> books = bookService.getBookByTextField(searchName);
        paging.setPaging(books);

        map.addAttribute("books", paging.firstPage());
        map.addAttribute("page", paging.getCurrentPage());
        map.addAttribute("pageCount", paging.getPageCount());

        if(!books.isEmpty()) {
            return "showBooks";
        }
        else {
            map.addAttribute("attribute", "book");
            return "searchFail";
        }
    }
}
