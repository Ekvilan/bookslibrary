package com.shribak.dao.impl;

import com.shribak.dao.FavouriteBookDao;
import com.shribak.entity.Book;
import com.shribak.entity.FavouriteBook;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FavouriteBookDaoImpl implements FavouriteBookDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(FavouriteBook book) {
        sessionFactory.getCurrentSession().save(book);
    }

    @Override
    public void delete(Integer id) {
        FavouriteBook book = (FavouriteBook) sessionFactory.getCurrentSession().load(FavouriteBook.class, id);

        if (null != book) {
            this.sessionFactory.getCurrentSession().delete(book);
        }
    }

    @Override
    public List<Book> getFavouriteBooks(Integer userId) {
        List<Book> favouriteBooks = new ArrayList<Book>();
        Query q = sessionFactory.getCurrentSession().createQuery("from FavouriteBook f where f.user.id = :userId");
        q.setInteger("userId", userId);
        List<FavouriteBook> list = q.list();
        for(FavouriteBook book : list) {
            favouriteBooks.add(book.getBook());
        }
        return favouriteBooks;
    }
}
