package com.shribak.dao.impl;

import com.shribak.dao.LastReadBookDao;
import com.shribak.entity.Book;
import com.shribak.entity.LastReadBook;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class LastReadBookDaoImpl implements LastReadBookDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(LastReadBook book) {
        checkIfExist(book);
        sessionFactory.getCurrentSession().save(book);
        checkSize(book.getUser().getId());
    }

    @Override
    public void delete(Integer id) {
        LastReadBook book = (LastReadBook) sessionFactory.getCurrentSession().load(LastReadBook.class, id);
        if (null != book) {
            this.sessionFactory.getCurrentSession().delete(book);
        }
    }

    @Override
    public List<Book> getLastReadBooks(Integer id) {
        List<Book> lastReadBooks = new ArrayList<Book>();
        List<LastReadBook> books = getByUserId(id);
        Collections.reverse(books);

        for(LastReadBook book : books) {
            lastReadBooks.add(book.getBook());
        }
        return lastReadBooks;
    }

    @Override
    public LastReadBook getBook(Integer userId, Integer bookId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from LastReadBook f where f.user.id = :userId and f.book.id = :bookId");
        query.setInteger("userId", userId);
        query.setInteger("bookId", bookId);

        return (LastReadBook)query.uniqueResult();
    }

    @Override
    public void update(LastReadBook book) {
        sessionFactory.getCurrentSession().update(book);
    }


    private List<LastReadBook> getByUserId(Integer id) {
        Query q = sessionFactory.getCurrentSession().createQuery("from LastReadBook f where f.user.id = :id");
        q.setInteger("id", id);
        return q.list();
    }


    private void checkIfExist(LastReadBook book) {
        List<LastReadBook> lastReadBooks = getByUserId(book.getUser().getId());

        if(lastReadBooks.contains(book)) {
            int index = lastReadBooks.indexOf(book);
            LastReadBook lastReadBook = lastReadBooks.get(index);
            delete(lastReadBook.getId());
        }
    }

    private void checkSize(Integer id) {
        List<LastReadBook> books = getByUserId(id);
        if(books.size() > LastReadBook.SIZE_LAST_READ_BOOKS) {
            delete(books.get(0).getId());
        }
    }
}
