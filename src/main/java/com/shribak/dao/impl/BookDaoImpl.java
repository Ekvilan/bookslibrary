package com.shribak.dao.impl;

import com.shribak.dao.BookDao;
import com.shribak.entity.Book;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDaoImpl implements BookDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Book book) {
        sessionFactory.getCurrentSession().save(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return sessionFactory.getCurrentSession().createQuery("from Book").list();
    }

    @Override
    public Book getBookById(Integer id) {
        Query q = sessionFactory.getCurrentSession().createQuery("from Book b where b.id = :id");
        q.setInteger("id", id);
        Book book = (Book)q.uniqueResult();
        return book;
    }

    @Override
    public Book getBookByName(String name) {
        Query q = sessionFactory.getCurrentSession().createQuery("from Book b where b.name = :name");
        q.setString("name", name);
        Book book = (Book)q.uniqueResult();
        return book;
    }

    @Override
    public List<Book> getBookByTextField(String name) {
        String searchName = "%" + name + "%";
        Query q = sessionFactory.getCurrentSession().createQuery("from Book b where b.name like :searchName");
        q.setString("searchName", searchName);

        return q.list();
    }

    @Override
    public List<Book> getBookByLetter(String let) {
        String name = let + "%";
        Query q = sessionFactory.getCurrentSession().createQuery("from Book b where b.name like :name ");
        q.setString("name", name);

        return q.list();
    }

    @Override
    public void deleteBook(Integer bookId) {
        Book book = (Book) sessionFactory.getCurrentSession().load(Book.class, bookId);

        if (null != book) {
            this.sessionFactory.getCurrentSession().delete(book);
        }
    }

    @Override
    public void update(Book book) {
        sessionFactory.getCurrentSession().update(book);
    }

    @Override
    public List<Book> getMostDownloadedBooks() {
        Query q = sessionFactory.getCurrentSession().createQuery("from Book order by downloadCount desc");
        q.setMaxResults(10);
        return q.list();
    }

    @Override
    public List<Book> getMostReadBooks() {
        Query q = sessionFactory.getCurrentSession().createQuery("from Book order by readCount desc");
        q.setMaxResults(10);
        return q.list();
    }

    @Override
    public List<Book> getLastUploadedBooks() {
        Query q = sessionFactory.getCurrentSession().createQuery("from Book order by uploadDate desc");
        q.setMaxResults(5);
        return q.list();
    }
}
