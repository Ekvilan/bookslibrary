package com.shribak.dao.impl;

import com.shribak.dao.UserDao;
import com.shribak.entity.User;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public User findById(Integer id) {
        return (User)sessionFactory.getCurrentSession().load(User.class, id);
    }

    @Override
    public User findByName(String name) {
        Query q = sessionFactory.getCurrentSession().createQuery("from User u where u.userName = :name");
        q.setString("name", name);

        return (User)q.uniqueResult();
    }

    @Override
    public User findByNameAndPassword(String name, String password) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                    "from User u where u.userName = :name and u.password = :password");
        q.setString("name", name);
        q.setString("password", password);

        return (User)q.uniqueResult();
    }

    @Override
    public void save(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public List<User> getUsers() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    @Override
    public void deleteUser(Integer userId) {
        User user = (User) sessionFactory.getCurrentSession().load(User.class, userId);

        if (null != user) {
            this.sessionFactory.getCurrentSession().delete(user);
        }
    }

    public List<User> getTopUploader(){
        Query q = sessionFactory.getCurrentSession().createQuery(
                "from User where username not like 'admin%' order by uploadedBooksCount desc");
        q.setMaxResults(5);
        return q.list();
    }

    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }
}

