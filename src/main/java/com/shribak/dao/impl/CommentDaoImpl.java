package com.shribak.dao.impl;

import com.shribak.dao.CommentDao;
import com.shribak.entity.Comment;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class CommentDaoImpl implements CommentDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }
}
