package com.shribak.dao;

import com.shribak.entity.User;

import java.util.List;

public interface UserDao  {
    public User findById(Integer id);
    public User findByName(String name);
    public User findByNameAndPassword(String name, String password);
    public void save(User user);
    public List<User> getUsers();
    public void deleteUser(Integer userId);
    public List<User> getTopUploader();
    public void update(User user);
}