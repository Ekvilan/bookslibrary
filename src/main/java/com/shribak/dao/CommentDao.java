package com.shribak.dao;


import com.shribak.entity.Comment;


public interface CommentDao {
    public void save(Comment comment);
}
