package com.shribak.dao;

import com.shribak.entity.Book;
import java.util.List;

public interface BookDao {
    public void save(Book book);
    public List<Book> getAllBooks();
    public Book getBookById(Integer id);
    public Book getBookByName(String name);
    public List<Book> getBookByTextField(String name);
    public List<Book> getBookByLetter(String let);
    public void deleteBook(Integer id);
    public void update(Book book);
    public List<Book> getMostDownloadedBooks();
    public List<Book> getMostReadBooks();
    public List<Book> getLastUploadedBooks();
}
