package com.shribak.dao;


import com.shribak.entity.Book;
import com.shribak.entity.FavouriteBook;

import java.util.List;

public interface FavouriteBookDao {
    public void add(FavouriteBook book);
    public void delete(Integer id);
    public List<Book> getFavouriteBooks(Integer userId);
}
