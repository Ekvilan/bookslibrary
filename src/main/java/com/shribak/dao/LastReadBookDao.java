package com.shribak.dao;

import com.shribak.entity.Book;
import com.shribak.entity.LastReadBook;

import java.util.List;

public interface LastReadBookDao {
    public void add(LastReadBook book);
    public void delete(Integer id);
    public List<Book> getLastReadBooks(Integer id);
    public LastReadBook getBook(Integer userId, Integer bookId);
    public void update(LastReadBook book);
}
