<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <style>
        <%@ include file="../css/jumbotron-narrow.css"%>
        <%@ include file="../css/bootstrap.min.css"%>
        <%@ include file="../css/readBook.css"%>
    </style>
</head>
<body>

    <%@ include file="header.jsp" %>
    <div class="container" >
        <%@ include file="actionPanel.jsp"%>
        <div class="border">
            <div class="text">
                ${bookPart}
            </div>
        </div>

        <ul class="pager">
            <li><a href="/lastBookPage">Previous</a></li>
            <li> &nbsp;&nbsp;&nbsp;&nbsp;page ${page} from ${pageCount}&nbsp;&nbsp;&nbsp;&nbsp; </li>
            <li><a href="/nextBookPage">Next</a></li>
        </ul>
    </div>

    <%@include file="footer.jsp"%>

</body>
</html>