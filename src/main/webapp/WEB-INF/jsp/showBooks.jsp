<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <style><%@ include file="../css/search.css"%></style>
</head>
<body>
    <%@ include file="header.jsp"%>

    <div class="container">
        <%@ include file="search.jsp"%>

        <table class="table table-bordered table-hover table-striped">
            <tr>
                <sec:authorize access="hasRole('ROLE_ADMIN')"><th>ID</th></sec:authorize>
                <th>TITLE</th>
                <th>CATEGORY</th>
                <th>AUTHOR</th>
                <th>UPLOAD_DATE</th>
            </tr>
            <c:forEach items="${books}" var="book">
                <tr>
                    <sec:authorize access="hasRole('ROLE_ADMIN')"><td>${book.id}</td></sec:authorize>
                    <td><a href="/aboutBook${book.id}">${book.name}</a></td>
                    <td>${book.category}</td>
                    <td>${book.author}</td>
                    <td>${book.uploadDate}</td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')"><td><a href="/showBooks/delete/${book.id}">delete</a></td></sec:authorize>
                </tr>
            </c:forEach>
        </table>

        <div class="paging">
            <ul class="pagination">
                <li><a href="/showBooks/firstPage">first&nbsp;&nbsp;</a></li>
                <li><a href="/showBooks/pageDown">&laquo;</a></li>
                <li><a href="#">&nbsp;&nbsp;&nbsp;${page}&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="/showBooks/pageUP">&raquo;</a></li>
                <li><a href="/showBooks/lastPage">&nbsp;&nbsp;last</a></li>
            </ul>
        </div>

    </div>

    <%@include file="footer.jsp"%>
</body>
</html>