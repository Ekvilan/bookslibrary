<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <style>
        <%@ include file="../css/upload.css"%>
        span.error {
            color: red;
        }
    </style>
</head>
<body>

    <%@include file="header.jsp"%>

    <div class="container">
        <div class="upload_form">
            <h3 class="h3_text" align="center">Select a file to upload.</h3><br>

            <form:form method="post" action="/saveBook" modelAttribute="book" enctype="multipart/form-data"
                       class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name:</label>
                    <div class="col-sm-6">
                        <input class="form-control input-sm" name="name" type="text" />
                    </div>
                    <span class="error"><form:errors path="name" /></span>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Author:</label>
                    <div class="col-sm-6">
                        <input class="form-control input-sm" name="author" type="text" />
                    </div>
                    <span class="error"><form:errors path="author" /></span>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Category:</label>
                    <div class="col-sm-6">
                        <select name="category" class="form-control">
                            <option value="0">Select</option>
                            <option value="Arts & Photography">Arts & Photography</option>
                            <option value="Detective">Detective</option>
                            <option value="Fantasy">Fantasy</option>
                            <option value="Biographies & Memoirs">Biographies & Memoirs</option>
                            <option value="Business & Money">Business & Money</option>
                            <option value="Classic">Classic</option>
                            <option value="Children's Books">Children's Books</option>
                            <option value="Christian Books & Bibles">Christian Books & Bibles</option>
                            <option value="Comics & Graphic Novels">Comics & Graphic Novels</option>
                            <option value="Computers & Technology">Computers & Technology</option>
                            <option value="Cookbooks, Food & Wine">Cookbooks, Food & Wine</option>
                            <option value="Crafts, Hobbies & Home">Crafts, Hobbies & Home</option>
                            <option value="Education & Teaching">Education & Teaching</option>
                            <option value="Engineering & Transportation">Engineering & Transportation</option>
                            <option value="Health, Fitness & Dieting">Health, Fitness & Dieting</option>
                            <option value="History">History</option>
                            <option value="Medical Books">Medical Books</option>
                            <option value="Mystery, Thriller & Suspense">Mystery, Thriller & Suspense</option>
                            <option value="Novel">Novel</option>
                            <option value="Other">Other</option>
                            <option value="Parenting & Relationships">Parenting & Relationships</option>
                            <option value="Politics & Social Sciences">Politics & Social Sciences</option>
                            <option value="Religion & Spirituality">Religion & Spirituality</option>
                            <option value="Romance">Romance</option>
                            <option value="Teen & Young Adult">Teen & Young Adult</option>
                        </select>
                    </div>
                </div>

                <br/>
                <div class="form-group">
                        <label class="col-sm-2 control-label">Select a file to upload:</label>
                    <div class="col-sm-8">
                        <input class="form-control input-sm" name="file" type="file" />
                    </div>
                </div>
                <div class="form_but">
                    <button type="submit" class="btn btn-primary" >Upload</button>
                </div>
                <span class="error"><form:errors path="file" /></span>
            </form:form>

        </div>
    </div>

    <%@include file="footer.jsp"%>

</body>
</html>