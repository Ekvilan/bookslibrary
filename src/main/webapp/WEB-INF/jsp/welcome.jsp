<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<meta http-equiv="refresh" content="3; url=/">
<html>
<head>
    <style>
        <%@ include file="../css/welcome.css"%>
    </style>
</head>
<body>
    <div id="container">
        <h1>You registered successfully on the Books Library!</h1>
        <h3>Enjoy reading.</h3>
        <br/>
        <div id="inside">
            <h4>
                <p>Your login:      ${user.userName}</p>
                <p>Your password:   ${pass}</p>
            </h4>
        </div>
    </div>
</body>
</html>