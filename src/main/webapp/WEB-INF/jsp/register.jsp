<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>

<html>
<head>
    <style>
        <%@ include file="../css/register.css"%>

        span.error {
            color: red;
        }
    </style>

</head>
<body>

    <%@include file="header.jsp"%>

    <div class="container">
        <div class="register_form">
            <h3 class="h3_text" align="center">Please Register</h3><br>

            <form:form class="form-horizontal" method="post" commandName="user">
                <div class="form-group">
                    <label for="userName" class="col-sm-2 control-label">Login:</label>
                    <div class="col-sm-6">
                        <form:input class="form-control input-sm" id="userName" path="userName" type="text" />
                    </div>
                    <span class="error"><form:errors path="userName" /></span>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password:</label>
                    <div class="col-sm-6">
                        <form:input class="form-control input-sm" id="password" path="password" type="password" />
                    </div>
                    <span class="error"><form:errors path="password" /></span>
                </div>

                <div class="form-group">
                    <label for="confirmPassword" class="col-sm-2 control-label">Confirm Password:</label>
                    <div class="col-sm-6">
                        <form:input class="form-control input-sm" id="confirmPassword" path="confirmPassword" type="password" />
                    </div>
                    <span class="error"><form:errors path="confirmPassword" /></span>
                </div>

                <div class="form_but">
                    <form:button type="submit" class="btn btn-primary" >Sing up</form:button>
                </div>
            </form:form>

        </div>
    </div>

    <%@include file="footer.jsp"%>

</body>
</html>