<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<meta http-equiv="refresh" content="4; url=/main">
<html>
<head>
    <style>
        <%@ include file="../css/search.css"%>
    </style>
</head>
<body>

    <%@include file="header.jsp"%>

    <div class="container">
        <%@ include file="file_upload_success.jsp" %>
    </div>

    <div class="empty"></div>

    <%@include file="footer.jsp"%>

</body>
</html>