<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title></title>
    <style>
        <%@ include file="../css/aboutBook.css"%>
        <%@ include file="../css/jumbotron-narrow.css"%>
        <%@ include file="../css/bootstrap.min.css"%>
    </style>
</head>
<body>

    <div class="container">
        <%@include file="header.jsp"%>

        <h3 align="center">${book.name}</h3>

        <div class="head">
            <div class="image">
                <img src="/image" width="90%" height="99%" />
            </div>
            <div class="inform">
                <br><br>
                <table>
                    <tr>
                        <td>Author: </td>
                        <td>&nbsp;${book.author}</td>
                    </tr>
                    <tr>
                        <td>Category: </td>
                        <td>&nbsp;${book.category}</td>
                    </tr>
                    <tr>
                        <td>Uploaded by: </td>
                        <td>&nbsp;${book.user.userName}</td>
                    </tr>
                    <tr>
                        <td>Upload Date: </td>
                        <td>&nbsp;${book.uploadDate}</td>
                    </tr>
                </table>
                <br>
                <table>
                    <tr>
                        <td>Downloaded: </td>
                        <td>&nbsp;${book.downloadCount} times </td>
                    </tr>
                    <tr>
                        <td>Read: </td>
                        <td>&nbsp;${book.readCount} times </td>
                    </tr>
                </table>
            </div>

            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <form action="/saveImage" method="post" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td><input type="file" name="file" /></td>
                            <td><input type="submit" /></td>
                        </tr>
                    </table>
                </form>
            </sec:authorize>

        </div>

        <div class="description">
            ${book.description}
        </div>

        <div class="action">
            <h3 align="center">
                <a href="read/${book.id}" class="label label-primary">read</a>
                <a href="/download/${book.id}" class="label label-primary">download</a>
            </h3>
        </div>

        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <form method="post" action="/aboutBook/addDescription"  modelAttribute="description">
                <table>
                    <tr>
                        <td><textarea rows = 6 cols = 113 placeholder="add description" name="description" type="text"></textarea></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="submit" /></td>
                    </tr>
                </table>
            </form>
        </sec:authorize>

        <div class="middle">
            Comments:
            <div class="comments">
                <c:forEach items="${comments}" var="comment">
                    <div class="comment">
                        <div class="user_info">
                            <div class="left">${comment.user.userName}</div>
                            <div class="right">${comment.date}</div>
                        </div>
                        <div class="message">${comment.content}</div>
                    </div>
                </c:forEach>
            </div>

            <div class="comment_form">
                <form method="post" action="/aboutBook/addComment"  modelAttribute="comment">
                    <table>
                        <tr>
                            <td><textarea rows = 6 cols = 113 placeholder="type your message" name="content" type="text"></textarea></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                    </table>
                </form>
            </div>

        </div>

    </div>

    <%@include file="footer.jsp"%>
</body>
</html>