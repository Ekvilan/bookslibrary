<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <style>
        <%@ include file="../css/search.css"%>
    </style>
</head>
<body>

<div class="search">
    <div class="div1">
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="А" /></c:url>">А</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Б" /></c:url>">Б</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="В" /></c:url>">В</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Г" /></c:url>">Г</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Д" /></c:url>">Д</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Е" /></c:url>">Е</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ё" /></c:url>">Ё</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ж" /></c:url>">Ж</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="З" /></c:url>">З</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="И" /></c:url>">И</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="К" /></c:url>">К</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Л" /></c:url>">Л</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="М" /></c:url>">М</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Н" /></c:url>">Н</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="О" /></c:url>">О</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="П" /></c:url>">П</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Р" /></c:url>">Р</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="С" /></c:url>">С</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Т" /></c:url>">Т</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="У" /></c:url>">У</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ф" /></c:url>">Ф</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Х" /></c:url>">Х</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ц" /></c:url>">Ц</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ч" /></c:url>">Ч</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Щ" /></c:url>">Щ</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Э" /></c:url>">Э</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Ю" /></c:url>">Ю</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Я" /></c:url>">Я</a>
        <br>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="A" /></c:url>">A</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="B" /></c:url>">B</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="C" /></c:url>">C</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="D" /></c:url>">D</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="E" /></c:url>">E</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="F" /></c:url>">F</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="G" /></c:url>">G</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="H" /></c:url>">H</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="I" /></c:url>">I</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="J" /></c:url>">J</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="K" /></c:url>">K</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="L" /></c:url>">L</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="M" /></c:url>">M</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="N" /></c:url>">N</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="O" /></c:url>">O</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="P" /></c:url>">P</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Q" /></c:url>">Q</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="R" /></c:url>">R</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="S" /></c:url>">S</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="T" /></c:url>">T</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="U" /></c:url>">U</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="V" /></c:url>">V</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="W" /></c:url>">W</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="X" /></c:url>">X</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Y" /></c:url>">Y</a>
        <a href="<c:url value="/searchBookByLetter"><c:param name="letter" value="Z" /></c:url>">Z</a>
    </div>
    <div class="div2">
        <div>
            <form  action="/searchBookByTextField" method="post">
                <div class="input-group input-group-sm">
                    <input type="text" size="40" name="searchBook" class="form-control" placeholder="Type book's name you are looking for">
                    <span class="input-group-btn">
                        <button class="btn btn-default active header" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br>

</body>
</html>