<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Books Library</title>
    <style>
        <%@ include file="../css/jumbotron-narrow.css"%>
        <%@ include file="../css/bootstrap.min.css"%>
    </style>
</head>
<body>
     <%@include file="header.jsp"%>

<div class="container">
    <div class="jumbotron">
        <h1>Books Library</h1>
        <p class="lead">The Application supports the books of the following formats: txt, rtf and fb2.</p>
        <h4>After easy registration you will be able to read and download books.</h4>
        <h4>Also you will be able to upload your own books.</h4>
    </div>

    <div class="row marketing">
        <div class="col-lg-6">
            <h4 align="center">The most read books</h4>
            <table class="table table-bordered table-hover table-striped table-condensed">
                <c:forEach items="${readBooks}" var="book">
                    <tr>
                        <td><a href="/aboutBook${book.id}">${book.name}</a></td>
                        <td>${book.category}</td>
                        <td>${book.readCount}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <div class="col-lg-6">
            <h4 align="center">The most downloaded books</h4>
            <table class="table table-bordered table-hover table-striped table-condensed">
                <c:forEach items="${downloadedBooks}" var="book">
                    <tr>
                        <td><a href="/aboutBook${book.id}">${book.name}</a></td>
                        <td>${book.category}</td>
                        <td>${book.downloadCount}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>

</div>

     <%@include file="footer.jsp"%>

</body>
</html>