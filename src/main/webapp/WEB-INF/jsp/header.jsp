<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>

<html>
<head>
    <title>Books Library</title>
    <style>
        <%@ include file="../css/jumbotron-narrow.css"%>
        <%@ include file="../css/bootstrap.min.css"%>
    </style>
</head>
<body>

<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li class="active"><a href="/">Home</a></li>
            <sec:authorize access="hasRole('ROLE_ADMIN')"><li><a href="/showUsers">All users</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')"><li><a href="/showBooks">All books</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_ANONYMOUS')"><li><a href="/register">Sign Up</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_ANONYMOUS')"><li><a href="/login">Log In</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')"><li><a href="/logout">Log out</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')"><li><a href="/upload">upload</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')"><li><a href="/showFavouriteBooks">favourite books</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')">
                <li><a href="#">(<%=SecurityContextHolder.getContext().getAuthentication().getName() %>)</a></li>
            </sec:authorize>
        </ul>
        <h3 class="text-muted">BooksLibrary</h3>
    </div>
</div>
</body>
</html>