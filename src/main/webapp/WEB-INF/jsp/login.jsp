<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <style>
        <%@ include file="../css/login.css"%>
        span.error {
            color: red;
        }
    </style>
</head>
<body>

    <%@include file="header.jsp"%>

    <div class="container">
        <div class="login_form">
            <h3 class="h3_text" align="center">Please Log in</h3><br>

            <form class="form-horizontal" action="j_spring_security_check" method="post">
                <div class="form-group">
                    <label for="j_username" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input class="form-control input-sm" id="j_username" name="j_username" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label for="j_password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input class="form-control input-sm" id="j_password" name="j_password" type="password">
                    </div>
                </div>

                <div class="form_but">
                    <button type="submit" class="btn btn-primary" >Log in</button>
                </div>
            </form>

            <span class="error">${message}</span>

        </div>
    </div>

    <%@include file="footer.jsp"%>

</body>
</html>