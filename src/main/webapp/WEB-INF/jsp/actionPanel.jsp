<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <style>
        <%@ include file="../css/jumbotron-narrow.css"%>
        <%@ include file="../css/bootstrap.min.css"%>
        <%@ include file="../css/readBook.css"%>
    </style>
</head>
<body>
    <div class="action_panel">
        <ul class="nav nav-pills nav-justified">
            <li><a href="/main">return for searching</a></li>
            <li><a href="/addFavouriteBook">add book to favourite</a></li>
            <li><a href="/savePage">save page</a></li>
        </ul>
    </div>
</body>
</html>
