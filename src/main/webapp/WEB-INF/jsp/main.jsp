<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <style>
        <%@ include file="../css/search.css"%>
    </style>
</head>
<body>

    <%@include file="header.jsp"%>

    <div class="container">
        <%@ include file="search.jsp" %>
        <br><br>

            <div class="row">
                <div class="col-6 col-sm-6 col-lg-4">
                    <h3>Recently read books</h3>
                    <table class="table table-bordered table-hover table-striped table-condensed">
                        <c:forEach items="${lastReadBooks}" var="book">
                            <tr>
                                <td><a href="/aboutBook${book.id}">${book.name}</a></td>
                                <%--<td>${book.author}</td>--%>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-6 col-sm-6 col-lg-4">
                    <h3>Last uploaded books</h3>
                    <table class="table table-bordered table-hover table-striped table-condensed">
                        <c:forEach items="${lastUploadedBooks}" var="book">
                            <tr>
                                <td>${book.name}</td>
                                <td>${book.category}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-6 col-sm-6 col-lg-4">
                    <h3>Top uploaders</h3>
                    <table class="table table-bordered table-hover table-striped table-condensed">
                        <c:forEach items="${topUploader}" var="user">
                            <tr>
                                <td>${user.userName}</td>
                                <td>${user.uploadedBooksCount}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <%@include file="footer.jsp"%>

</body>
</html>