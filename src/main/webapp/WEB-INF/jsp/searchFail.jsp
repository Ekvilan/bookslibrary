<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>

<html>
<head>
</head>
<body>
    <%@include file="header.jsp"%>

    <div class="container">
        <%@ include file="search.jsp" %>
        <br><br>
        <h4 align="center">No ${attribute} found</h4>

        <%@ include file="empty.jsp" %>
    </div>

    <%@include file="footer.jsp"%>
</body>
</html>