<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <style><%@ include file="../css/search.css"%></style>
</head>
<body>
<%@ include file="header.jsp"%>

<div class="container">
    <%@ include file="search.jsp"%>

    <table class="table table-bordered table-hover table-striped">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>PASSWORD</th>
            <th>REGISTRATION DATE</th>
        </tr>
        <c:forEach items="${users}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.userName}</td>
                <td>${user.password}</td>
                <td>${user.registrationDate}</td>
                <td><a href="/showUsers/delete/${user.id}">delete</a></td>
            </tr>
        </c:forEach>
    </table>

    <div class="paging">
        <ul class="pagination">
            <li><a href="/showUsers/firstPage">first&nbsp;&nbsp;</a></li>
            <li><a href="/showUsers/pageDown">&laquo;</a></li>
            <li><a href="#">&nbsp;&nbsp;&nbsp;${page}&nbsp;&nbsp;&nbsp;</a></li>
            <li><a href="/showUsers/pageUP">&raquo;</a></li>
            <li><a href="/showUsers/lastPage">&nbsp;&nbsp;last</a></li>
        </ul>
    </div>

</div>

<%@include file="footer.jsp"%>
</body>
</html>